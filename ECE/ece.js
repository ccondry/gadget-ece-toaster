var finesse = finesse || {};
finesse.gadget = finesse.gadget || {};
/** @namespace */
finesse.modules = finesse.modules || {};
finesse.modules.EmbeddedWebAppGadget = (function($) {
    var _clientLogger = finesse.cslogger.ClientLogger; // declare _clientLogger
    var _urlToLoad, _cfg, _LoggedInUser,_prefs;
	var _maxAvailableHeight;
	var _currentView = 'default';
    var _defaultHeight = 600;
    var _frameHeightOffset = 20;
	var _activityData;
	var gadgetLoaded = false;
	var _user = null, _dialogs = null, _dialog = null;
	var _queuedPostMessages = [];
	var _userId;
	var _xEgainSession;
	var _xCsrfToken;
	var _logoutSuccessfulFlag = false;
	var _sleepTime = 5000;


// the ECE_Chat MRD ID
  var chatMrdId = '5004';
  var emailMrdId = '5005';
  var media, mediaDialogs, mediaList,
      /**
       * Prefix for call variables in dialog.mediaProperties
       */
      CALL_VARIABLE_PREFIX = "callVariable",
      /**
       * Store the maxDialogLimit, interruptAction, and dialogLogoutAction options to be used with this gadget's
       * media object.
       */
      mediaOptions;

	var _cobrowseActivityId;
	var _eceUserId;

	_resizeUI = function () {
		_clientLogger.log("ECE Gadget :: Max height: " + _maxAvailableHeight + ", Default height: " + _defaultHeight);
		if (_currentView === 'default') {
			gadgets.window.adjustHeight(_defaultHeight);
			$("#FileFrame").css('height', _defaultHeight - _frameHeightOffset);
		} else if (_currentView === 'canvas') {
			gadgets.window.adjustHeight(_maxAvailableHeight);
			$("#FileFrame").css('height', _maxAvailableHeight - _frameHeightOffset);
		}
    };

	var _dataRequestHandler = function (topic, data) {
        var dataCopy;
        _clientLogger.log("ECE Gadget :: in function _dataRequestHandler");
        //Ensure a valid data object with "type" and "data" properties.
        if (typeof data === "object" &&
            typeof data.type === "string" &&
            typeof data.data === "object") {

			_clientLogger.log("ECE Gadget :: message received with type: " + data.type);
            switch (data.type) {
                case "ECEMessageType":
					_clientLogger.log("ECE Gadget :: message received with payload: " + JSON.stringify(data.data));
                    break;
				case "Failure":
					_clientLogger.log("ECE Gadget :: Error in HTTP request to KB, HTTP response code: " + data.data.code);
					_showToasterNotification(_prefs.getMsg("L10N_ERROR_TITLE"), _prefs.getMsg("L10N_UNABLE_TO_CONNECT_KB"));
					_postMessageToChildFrame("showAdvertisement", "reason");
					break;
				case "Success":
					_clientLogger.log("ECE Gadget :: Successful HTTP request to KB, HTTP response code: " + data.data.code);
					var data = {
						type: "ShowNotificationSolve",
						data: {},
						invokeID: (new Date()).getTime()
					};
					gadgets.Hub.publish("ECETopic.info", data);
					break;
				case "AddToReplyPane":
					_clientLogger.log("ECE Gadget :: Got message for AddToReplyPane");
					finesse.containerservices.ContainerServices.activateMyTab();
					var queuedMsg = {"action": "AddToReplyPane","content" : data.data.content}
					_queuedPostMessages[_queuedPostMessages.length] = queuedMsg;
					break;
				case "CobrowseLicenseAvailable":
					_clientLogger.log("ECE Gadget :: Got message for CobrowseLicenseAvailable");
					_postMessageToChildFrame("CheckCobrowse", "CheckCobrowse");
					break;
				case "MapActivityWithCBSession":
					_clientLogger.log("ECE Gadget :: Got message for MapActivityWithCBSession");
					_postMessageToChildFrame("MapActivityWithCBSession", data.data);
					break;
				case "CobrowseSessionJoined":
					_clientLogger.log("ECE Gadget :: Got message for CobrowseSessionJoined");
					_postMessageToChildFrame("CobrowseSessionJoined", data.data);
					break;
				case "CobrowseSessionEnded":
					_clientLogger.log("ECE Gadget :: Got message for CobrowseSessionEnded");
					_postMessageToChildFrame("CobrowseSessionEnded", data.data);
					break;
            }
        }
    };

	_postMessageToChildFrame = function(action, content)
	{
		var iFrame = document.getElementById('FileFrame');
		var innerDoc = iFrame.contentWindow;
		var postObj = new Object();
		postObj.action = action;
		postObj.content = content;
		_clientLogger.log("ECE Gadget :: postObj: " + postObj.action + ", " + postObj.content);
		innerDoc.postMessage(postObj, "*");
		_clientLogger.log("ECE Gadget :: message posted to child frame");
	}

	_showToasterNotification = function(titleText, text) {
		var notification = new Notification(titleText, {
			dir: "auto",
			body: text
		});
	};

    /*
     * The following will be used to check if the tab is currently not visible. We
     * need to do this because there is no way currently of being notified when the
     * User navigates away. This allows us to do things on our Gadget if this is so.
     */
    _checkForTabNotVisible = function() {
        if (finesse.containerservices.ContainerServices.tabVisible()) {
            window.setTimeout(_checkForTabNotVisible, 100);
        } else {
            _clientLogger.log("ECE Gadget :: _checkForTabNotVisible(): Tab is no longer visible. Stopped looking.");
        }
    };

    /*
     * The following runs when the Tab becomes active. Here we initialize the contents
     * of the iFrame containing our Gadget.
     */
    _handleTabVisible = function() {
        _clientLogger.log("ECE Gadget :: _handleTabVisible()");
		if (_queuedPostMessages.length > 0)
		{
			for (var i = 0; i < _queuedPostMessages.length; i++)
			{
				_clientLogger.log("ECE Gadget :: adding text to editor: " + _queuedPostMessages[i]);
				var queuedMsg = _queuedPostMessages[i];
				_postMessageToChildFrame(queuedMsg.action, queuedMsg.content);
			}

			_queuedPostMessages = [];
		}

		if (gadgetLoaded)
			return;

		successCallback = function(rsp) {
			var displayOut = $("#FileFrame").contents().find("div");
			try
			{
				var jsonRes = JSON.parse(rsp.text);
				if(typeof jsonRes.message != 'undefined' && jsonRes.message != "")
				{
					_clientLogger.log("ECE Gadget :: Show message");
					displayOut.html(_prefs.getMsg(jsonRes.message));
				}
			}
			catch(err)
			{
				if (typeof rsp.text != 'undefined' && $.trim(rsp.text) != "")
				{
					displayOut.html(rsp.text);
					if (displayOut.find('form').length > 0)
					{
						 _clientLogger.log("ECE Gadget :: Submitted sendRequestToIDPForm");
						 displayOut.find('form').submit();
						 gadgetLoaded = true;
					}
					else
					{
						_clientLogger.log("ECE Gadget :: Form not found");
					}
				}
				else
				{
					displayOut.html(_prefs.getMsg("L10N_UNEXPECTED_ERROR"));
				}

			}

			_resizeUI();
			_clientLogger.log("ECE Gadget :: Starting the _checkForTabNotVisible() checker...");

			window.setTimeout(_checkForTabNotVisible, 100);
		};

        //Getting user through gadgets.io.makeRequest api
        var params = {};
        params[gadgets.io.RequestParameters.HEADERS] = {};

        /**
         * For SSO deployments , Authorization string will start with "bearer"
         * string followed by access token.This utility method can be used to get the complete
         * authorization header string based on authorization mode.The authorization mode will
         * be accessed from the config object.
         */
		if (_cfg.systemAuthMode === finesse.utilities.Utilities.getAuthModes().SSO)
		{
			_clientLogger.log("ECE Gadget :: isSSOagent=true...");
			_urlToLoad = _urlToLoad + "&isSSOagent=true";
		}
		else
		{
			_clientLogger.log("ECE Gadget :: isSSOagent=false...");
			_urlToLoad = _urlToLoad + "&isSSOagent=false";
		}

		_urlToLoad = _urlToLoad+"&"+new Date().getTime();

		params[gadgets.io.RequestParameters.HEADERS].Authorization = finesse.utilities.Utilities.getAuthHeaderString(_cfg);

		gadgets.io.makeRequest(encodeURI(_urlToLoad), successCallback, params);

        _resizeUI();
    };

	/*
     * The following runs when the Gadget View Changes.  Here we will adjust the Gadget Height and
     * store off the view.
     */
	_handleGadgetViewChanged = function (gadgetViewChangedEvent) {
		_clientLogger.log("ECE Gadget :: in function _handleGadgetViewChanged");
		var gadgetId = finesse.containerservices.ContainerServices.getMyGadgetId();
		var tabId = finesse.containerservices.ContainerServices.getMyTabId();

		_clientLogger.log("ECE Gadget :: in function _handleGadgetViewChanged: gadgetId -> " + gadgetId + ", tabId -> " + tabId);

		if (gadgetViewChangedEvent.getGadgetId() === gadgetId && gadgetViewChangedEvent.getTabId() === tabId) {
			_currentView = gadgetViewChangedEvent.getView();

			_clientLogger.log("ECE Gadget :: in function _handleGadgetViewChanged: _currentView -> " + _currentView);
			if (gadgetViewChangedEvent.getView() === 'canvas') {
				_maxAvailableHeight = gadgetViewChangedEvent.getMaxAvailableHeight();
			}
			_clientLogger.log("ECE Gadget :: in function _handleGadgetViewChanged: _maxAvailableHeight -> " + _maxAvailableHeight);

			_resizeUI();
		}
	};

    /*
     * The following runs when the Max Available Height Changes.  Here we will adjust the Gadget Height.
     */
	_handleMaxAvailableHeightChange = function (maxAvailableHeightChangeEvent) {
		_clientLogger.log("ECE Gadget :: in method _handleMaxAvailableHeightChange");
		if (_currentView === 'canvas') {
			_maxAvailableHeight = maxAvailableHeightChangeEvent.getMaxAvailableHeight();
			_resizeUI();
		}
	};

    _handleUserLoad = function (userevent) {
		_clientLogger.log("ECE Gadget :: In function _handleUserLoad");

        _dialogs = _user.getDialogs( {
            onCollectionAdd : _handleDialogAdd,
            onCollectionDelete : _handleDialogDelete,
			onLoad : _handleDialogsLoaded
        });

    mediaList = _user.getMediaList( {
        onLoad: handleMediaListLoad
    });
        _resizeUI();
    };

	_handleDialogAdd = function (dialog)
	{
		_clientLogger.log("ECE Gadget :: In function _handleDialogAdd");
		var query = "";
		//Add call type to search query
		var dataObject = {};
		dataObject.calltype = dialog.getCallType();

		//Add defined call variables to search query
		for (var i=1; i < 11; i++)
		{
			var result = eval("dialog.getMediaProperties().callVariable" + i + "");
			if (result != null)
				eval("dataObject.Callvariable" + i + " = '" + result + "'");
		}

		_clientLogger.log("ECE Gadget :: Full search query: " + query);
		if (gadgets.Hub)
		{
			var data = {
				type: "SolveCall",
				data: { "params" : dataObject},
				invokeID: (new Date()).getTime()
			};

			gadgets.Hub.publish("ECETopic.info", data);
		}
	};

	_handleDialogDelete = function (o)
	{
		_clientLogger.log("ECE Gadget :: In function _handleDialogDelete");
	};

	_handleDialogsLoaded = function (o)
	{
		_clientLogger.log("ECE Gadget :: In function _handleDialogsLoaded");
	};

	_handleUserChange = function (o)
	{
		if(o._data.state =="LOGOUT")
		{
			successCallbackLogout = function(rsp) {
					try
					{
							var response = JSON.parse($.trim(rsp.text));
							if (response.logoutStatus == "1")
							{
									_clientLogger.log("ECE Gadget :: LogOut Successful");
							}
							else
							{
									_clientLogger.log("ECE Gadget :: LogOut Failed");
							}
					}
					catch(err)
					{
							_clientLogger.log("ECE Gadget :: LogOut Failed.. "+err.message);
					}

					_clientLogger.log("ECE Gadget :: End of successCallbackLogout");
			};

			//Getting user through gadgets.io.makeRequest api
			var params = {};
			params[gadgets.io.RequestParameters.HEADERS] = {};
			_eceUserId = sessionStorage.getItem("eceUserId");
			var urlLogout = web_server_protocol + "://" + web_server_name + "/" + web_context_root + "/web/integration/view/platform/common/login/logout.jsp?userId="+_eceUserId+"&finesse=1&"+new Date().getTime();
			_clientLogger.log("ECE Gadget :: Logging out the agent: " + urlLogout);
			gadgets.io.makeRequest(encodeURI(urlLogout), successCallbackLogout, params);
		}

		_clientLogger.log("ECE Gadget :: In function _handleUserChange");
  }

  /**
   * Handler that is called anytime a dialog is added for the user on all media.
   */
  handleMediaDialogsAdd = function (dialog) {
      //this gets called whenever there's a dialog change for all media
      //only update if the notification is for this gadgets's specific media
      //since there could be multiple media gadgets corresponding to a different media id
      _clientLogger.log("ECE Gadget :: In function handleMediaDialogsAdd");
      console.log('ECE Gadget :: dialog._data.mediaProperties.mediaId = ', dialog._data.mediaProperties.mediaId)
      let mrdId = dialog._data.mediaProperties.mediaId;
      if(mrdId === chatMrdId) {
          let toasterTitle = 'New Chat Arrived';
          let toasterBody = 'You have a new Chat waiting in the ECE gadget.';
          let toasterIcon = '/3rdpartygadget/files/icons/chat.png';

          // pop toaster notification
          let notification = new Notification(toasterTitle, {
            icon : toasterIcon,
            body : toasterBody,
            showWhenVisible: true,
            autoClose: false,
          });

          notification.onclick = function () {
            // show the finesse window when clicked
            parent.focus();
            // set the location hash of the finesse browser tab to the
            // TaskManagement gadget to focus on that gadget tab
            containerServices.activateMyTab();
            // and dismiss this notification
            this.close();
          };
          console.log('cccc notification=', notification);
      } else if (mrdId === emailMrdId) {
        let toasterTitle = 'New Email Arrived';
        let toasterBody = 'You have a new Email waiting in the ECE gadget.';
        let toasterIcon = '/3rdpartygadget/files/icons/email.png';

        // pop toaster notification
        let notification = new Notification(toasterTitle, {
          icon : toasterIcon,
          body : toasterBody,
          showWhenVisible: true,
          autoClose: false
        });
        notification.onclick = function () {
          // show the finesse window when clicked
          parent.focus();
          // set the location hash of the finesse browser tab to the
          // TaskManagement gadget to focus on that gadget tab
          containerServices.activateMyTab();
          // and dismiss this notification
          this.close();
        };
        console.log('cccc notification=', notification);
      }
  },
  /**
   * Load the current user's dialogs.
   * @return {undefined}
   */
  loadMediaDialogs = function (_media) {
    _clientLogger.log("ECE Gadget :: loadMediaDialogs(): loading media dialogs...")
      mediaDialogs = _media.getMediaDialogs({
          onCollectionAdd : handleMediaDialogsAdd,
          // onCollectionDelete : handleMediaDialogsDelete,
          onLoad: handleMediaDialogsLoad
      });
  },

  handleMediaDialogsLoad = function() {
      //not getting here
      var dialogCollection = mediaDialogs.getCollection(), id;

      for (id in dialogCollection) {
          var dialog = dialogCollection[id];
          handleMediaDialogsAdd(dialog);
      }
  },
  /**
   * Handler for the onLoad of a User object.  This occurs when the User object is initially read
   * from the Finesse server.  Any once only initialization should be done within this function.
   */
   // use the ECE gadget's _handleUserLoad method
  // handleUserLoad = function (_user) {
  //     mediaList = _user.getMediaList( {
  //         onLoad: handleMediaListLoad
  //     });
  // },

  /**
   * Handler for the onLoad of a MediaList object.  This occurs when the MediaList object is initially read
   * from the Finesse server.
   */
  handleMediaListLoad = function (_mediaList) {
    _clientLogger.log("ECE Gadget :: handleMediaListLoad(): Adding handler for loading the media from finesse...")
      try {
          //get the media with the specified id
          media = _mediaList.getMedia({
              id: chatMrdId,
              onLoad: handleMediaLoad,
              // onError: handleMediaError,
              // onChange: handleMediaChange,
              mediaOptions: mediaOptions
          });

          emailMedia = _mediaList.getMedia({
              id: emailMrdId,
              onLoad: handleMediaLoad,
              // onError: handleMediaError,
              // onChange: handleMediaChange,
              mediaOptions: mediaOptions
          });
      } catch ( error ) {
          // alert("media Channel Not Found: " + mrdID);
      }
  },

  /**
   * Utility function that returns an array of key-value pairs
   * for the query parameters in a given URL.
   */
  getUrlVars = function (url) {
      var vars = {};
      var parts = url.replace(/[?&]+([^=&]+)=([^&]*)/gi,
          function (m, key, value) {
              vars[key] = value;
          });
      return vars;
  },

  /**
   *	Callback used upon the load of a media object. This should be used for anything that only needs to be
   *  executed once during initialization.
   */
  handleMediaLoad = function(_media) {
      loadMediaDialogs(_media);
      //if user signed out of this media or state is unknown, show the sign in button
      // if (_media.getState() === states.LOGOUT || _media.getState() === undefined ) {
      //     showLogin()
      // } else {
      //     _media.refresh();
      // }
	};

    /** @scope finesse.modules.EmbeddedWebAppGadget */
    return {
        /**
         * Performs all initialization for this gadget
         */
        init: function(urlToLoad) {

      //First get just the URI for this gadget out of the full finesse URI and decode it.
      var gadgetURI = decodeURIComponent(getUrlVars(location.search)["url"]);

      //Now get the individual query params from the gadget URI
      var decodedGadgetURI = getUrlVars(gadgetURI);
      // set the chat and email MRD IDs using the gadget query params from finesse layout, if they were set
      chatMrdId = decodedGadgetURI["chatmrd"] || chatMrdId;
      emailMrdId = decodedGadgetURI["emailmrd"] || emailMrdId;

      let interruptAction = 'ignore'
      let dialogLogoutAction = 'close'
      mediaOptions = {
          maxDialogLimit: 10,
          interruptAction: interruptAction.toUpperCase(),
          dialogLogoutAction: dialogLogoutAction.toUpperCase()
      };

                _urlToLoad = urlToLoad;

                //For SSO deployment, Gadgets should use the finesse provided config
                //object instead of creating their own instance
                _cfg = finesse.gadget.Config;

                // Initiate the ClientServices and load the config object. ClientServices
                // are initialized with a reference to the current configuration.
                // For SSO deployments , gadgets need to initialize clientservices for
                // loading config object properly.Without initialization, SSO mode will not work
                finesse.clientservices.ClientServices.init(_cfg);
				_prefs = new gadgets.Prefs();
                _clientLogger.init(gadgets.Hub, "EmbeddedWebAppGadget", finesse.gadget.Config); //this gadget id will be logged as a part of the message

				_clientLogger.log("ECE Gadget :: Following constants defined: TIMEOUT=" + timeout + ", ece_server_url=" + ece_server_url + ", urlToLoad=" + _urlToLoad);

                _clientLogger.log("ECE Gadget :: init(): ECE gadget..Initializing Container Services...");
                containerServices = finesse.containerservices.ContainerServices.init();

                _clientLogger.log("ECE Gadget :: init(): Adding Tab Visible Handler...")
                containerServices.addHandler(finesse.containerservices.ContainerServices.Topics.ACTIVE_TAB, _handleTabVisible);

				_clientLogger.log("ECE Gadget :: init(): Adding GadgetViewChanged Handler...")
				containerServices.addHandler(finesse.containerservices.ContainerServices.Topics.GADGET_VIEW_CHANGED_EVENT, _handleGadgetViewChanged);

				_clientLogger.log("ECE Gadget :: init(): Adding MaxAvailableHeightChange Handler...")
				containerServices.addHandler(finesse.containerservices.ContainerServices.Topics.MAX_AVAILABLE_HEIGHT_CHANGED_EVENT, _handleMaxAvailableHeightChange);

                finesse.containerservices.ContainerServices.makeActiveTabReq();
				_dialogs = [];
				_dialog = null;

				_user = new finesse.restservices.User({
					id: finesse.gadget.Config.id,
					onLoad : _handleUserLoad,
					onChange : _handleUserChange
				});


				if (gadgets.Hub) {
					gadgets.Hub.subscribe("ECETopic.info", _dataRequestHandler);
					_clientLogger.log("ECE Gadget :: Subscribed to topic ECETopic.info");
				}

				var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
				var eventer = window[eventMethod];
				var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

				eventer(messageEvent,function(e) {
					var messageObj = e.data;
					_clientLogger.log("ECE Gadget :: Received an event message");
					if (!messageObj || !messageObj.messageType)
						return;

					if (messageObj.messageType == "ContextService")
					{
						_clientLogger.log("ECE Gadget :: Received activityId to Context Service gadget: " + messageObj.activityId);
						_clientLogger.log("ECE Gadget :: Received podId to Context Service gadget: " + messageObj.podId);

						if (typeof ContextService !== 'undefined')
						{
							if (messageObj.podId != null && messageObj.podId.length > 0)
								ContextService.GadgetControl.showPodById( "" + messageObj.podId);
							else
								ContextService.GadgetControl.showPodByQuery( { ECE_Activity_ID : messageObj.activityId + "" } );
						}

						if (parent.$("#tablink_KB").length > 0)
							_postMessageToChildFrame("CheckSolve", "CheckSolve");
						
						// Publish message to check cobrowse license
						var data = {
							type: "CheckCobrowseLicense",
							data: {},
							invokeID: (new Date()).getTime()
						};

						gadgets.Hub.publish("ECETopic.info", data);
					}
					else if (messageObj.messageType == "CacheUserId")
					{
						_clientLogger.log("ECE Gadget :: Got user ID to cache with ID: " + messageObj.userId);
						_userId = messageObj.userId;
						_xEgainSession = messageObj.sessionId;
						_xCsrfToken = messageObj.sessionToken;
					}
					else if (messageObj.messageType == "Solve")
					{
						_clientLogger.log("ECE Gadget :: Message Type: Solve");
						_activityData = messageObj.activityData;

						if (parent.$("#tablink_KB").length == 0)
						{
							_clientLogger.log("ECE Gadget :: Cannot solve, the KB gadget tab is not there in Finesse");

							_postMessageToChildFrame("showAdvertisement", "reason");
						}

						if (gadgets.Hub) {
							_clientLogger.log("ECE Gadget :: checked gadgets.Hub, publishing a message: " + JSON.stringify(messageObj.activityData));

							var data = {
								type: "Solve",
								data: { "activityData" : _activityData},
								invokeID: (new Date()).getTime()
							};

							gadgets.Hub.publish("ECETopic.info", data);
						}
					}
					else if (messageObj.messageType == "Cobrowse")
					{
						_clientLogger.log("ECE Gadget :: Message Type: Cobrowse");
						_cobrowseActivityId = messageObj.activityId;

						if (gadgets.Hub) {
							_clientLogger.log("ECE Gadget :: checked gadgets.Hub, publishing Cobrowse message: " + JSON.stringify(messageObj.activityId));

							var data = {
								type: "Cobrowse",
								data: { "activityId" : _cobrowseActivityId,
										"userScreenName" : messageObj.userScreenName
								},
								invokeID: (new Date()).getTime()
							};

							gadgets.Hub.publish("ECETopic.info", data);
						}
					}
					else if (messageObj.messageType == "ChatActivityLeaveOrComplete")
					{
						_clientLogger.log("ECE Gadget :: Message Type: ChatActivityLeaveOrComplete");
						_cobrowseActivityId = messageObj.activityId;

						if (gadgets.Hub) {
							_clientLogger.log("ECE Gadget :: checked gadgets.Hub, publishing Cobrowse message: " + JSON.stringify(messageObj.activityId));

							var data = {
								type: "ChatActivityLeaveOrComplete",
								data: { "activityId" : _cobrowseActivityId},
								invokeID: (new Date()).getTime()
							};

							gadgets.Hub.publish("ECETopic.info", data);
						}
					}
					else if (messageObj.messageType == "ECELoginInfo")
					{
							_clientLogger.log("ECE Gadget :: Got user ID to cache with ID: " + messageObj.userId);

							_clientLogger.log("ECE Gadget :: Posting message to CheckSolve");

							if (parent.$("#tablink_KB").length > 0)
								_postMessageToChildFrame("CheckSolve", "CheckSolve");
							
							// Publish message to check cobrowse license
							var data = {
								type: "CheckCobrowseLicense",
								data: {},
								invokeID: (new Date()).getTime()
							};

							gadgets.Hub.publish("ECETopic.info", data);

							_eceUserId = messageObj.userId;
							var data = {
								type: "CacheUserId",
								data: { "userId" : _eceUserId},
								invokeID: (new Date()).getTime()
							};

							gadgets.Hub.publish("ECETopic.info", data);
							sessionStorage.setItem("eceUserId",_eceUserId);
					}
				},false);
            } // init function
    }; // return
}(jQuery));